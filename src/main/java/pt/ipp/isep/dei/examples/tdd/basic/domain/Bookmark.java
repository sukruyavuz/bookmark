package pt.ipp.isep.dei.examples.tdd.basic.domain;

import java.util.ArrayList;

public class Bookmark {
    private int rating = 0;
    private ArrayList<String> tags = new ArrayList<>();

    public Integer increaseRating(){
        return this.rating = rating+1;
    }

    public Integer tagBookmark(String tag){
        tags.add(tag);
        return 99;
    }

    public Integer rmTagBookmark(String tag){
        tags.remove(tag);
        return 99;
    }
    public ArrayList<String> getTags(){
        return tags;
    }

    public Integer getRating(){
        return rating;
    }


}